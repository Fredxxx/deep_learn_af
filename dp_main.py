import tensorflow as tf
import matplotlib.pyplot as plt

# load data
mnist = tf.keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = mnist.load_data()


## define network structure 2 layer
# normalise data to 1 -> easier for network
x_train = tf.keras.utils.normalize(x_train, axis = 1)
x_train = x_train[0:10000]
y_train = y_train[0:10000]
x_test = tf.keras.utils.normalize(x_test, axis = 1)
# define model (there are two types sequential and ???)
model = tf.keras.models.Sequential()
# INPUT LAYER flatten data before feeding into network. Does not need to be a layer.
model.add(tf.keras.layers.Flatten(input_shape=(28, 28)))
# FIRST LAYER is density layer with 128 units (neurons) and activation = activation function
# as activation we use rectified linear (default to go), kind of probability distribution
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
# SECOND LAYER
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu)) 
# OUTPUT LAYER less neurons -> results
model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))


## training of network
# define parameters of training of the model
# optimizer, most complex part of network -> did not get it
# loss, kind of error, network tries to minimize loss. Very imortatnt for performance
model.compile(optimizer = 'adam',
              loss = 'sparse_categorical_crossentropy',
              metrics = ['accuracy'])
# train model
model.fit(x_train, y_train, epochs=1)
model.save('20190401')
#print(x_train.shape)
## show some stuff
#plt.imshow(x_train[8], cmap = plt.cm.binary)
#print(y_train[8])
#plt.show()
