import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
# load data
mnist = tf.keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = mnist.load_data()

#x_train = tf.keras.utils.normalize(x_train, axis = 1)
x_test = tf.keras.utils.normalize(x_test, axis = 1)

new_model = tf.keras.models.load_model('20190401')

#print(x_test.shape)

#val_loss, val_acc = new_model.evaluate(x_test, y_test)
#print(new_model.apply(x_test[5]))
#print(val_loss)
#print(val_acc)


predicts = new_model.predict([x_test])
print(np.argmax(predicts[356]))

plt.imshow(x_test[356], cmap = plt.cm.binary)
plt.show()
