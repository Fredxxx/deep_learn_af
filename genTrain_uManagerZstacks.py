from PIL import Image
import matplotlib.pyplot as plt
import numpy as np
import javabridge
import bioformats as bf
javabridge.start_vm(class_path=bf.JARS)

filename = "C:\\Users\\STORM\\data\\20190331_dpTrainingData\\train_1_1\\train_1_1_MMStack_Pos0.ome.tif"
#image = bioformats.BioformatsReader(filename)
md = bf.get_omexml_metadata(filename)
rdr = bf.ImageReader(filename, perform_init=True)
ome = bf.OMEXML(md)
pixels = ome.image().Pixels
print('pixels in Z: ' + str(pixels.SizeZ))
print('pixels in C: ' + str(pixels.SizeC))
print('pixels in T: ' + str(pixels.SizeT))
print('pixels in X: ' + str(pixels.SizeX))
print('pixels in Y: ' + str(pixels.SizeY))
#print(ome.image_count)
iome0 = ome.image(0)
with bf.ImageReader(filename) as reader:
    img = reader.read(t = 50)
    plt.imshow(img, cmap = plt.cm.binary)
    plt.show() 
    reader.close()
    


#pix = np.array(iome0).reshape(pixels.SizeX, pixels.SizeY, 1)
#x_train = np.array(iome0)
#plt.imshow(x_train, cmap = plt.cm.binary)
#plt.show()
#iome1 = ome.image(1)
#im=Image.open(filename)
#x_train = numpy.array(im)
#print(x_train.shape)
## show some stuff
#plt.imshow(x_train, cmap = plt.cm.binary)
#plt.show()   

javabridge.kill_vm()
